import org.bitbucket.eunjeon.seunjeon.Analyzer;
import org.bitbucket.eunjeon.seunjeon.LNode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by parallels on 8/15/15.
 */
public class LongSentenceFromfile {
    public static void main(String args[]) throws IOException {
        List<LNode> result = Analyzer.parseJava("사전 로딩.");
        System.out.println("result = " + result);

        String text = readFileAsString("./outofmemory.txt");
        System.out.println("text = " + text);
        result = Analyzer.parseJava(text);
        for (LNode term: result) {
            System.out.println(term);
        }
    }

    private static String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[10240];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }
}
