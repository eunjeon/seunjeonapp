import edu.jhu.nlp.wikipedia.PageCallbackHandler;
import edu.jhu.nlp.wikipedia.WikiPage;
import edu.jhu.nlp.wikipedia.WikiXMLParser;
import edu.jhu.nlp.wikipedia.WikiXMLParserFactory;
import org.bitbucket.eunjeon.seunjeon.Analyzer;
import org.bitbucket.eunjeon.seunjeon.LNode;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by parallels on 8/14/15.
 */
public class WikiPages {
    public static int docCount = 0;
    public static PrintWriter writer;
    static {
        try {
            writer = new PrintWriter("./output.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        WikiXMLParser wxsp = WikiXMLParserFactory.getSAXParser("./kowiki-20150807-pages-articles.xml.bz2");

        try {
            wxsp.setPageCallback(new PageCallbackHandler() {
                public void process(WikiPage page) {
                    docCount += 1;
                    System.out.println("docCount = " + docCount);
                    System.out.println("page.getTitle() = " + page.getTitle());
                    try {
                        List<LNode> result = Analyzer.parseJava(page.getText());
                        for (LNode node : result) {
                            writer.println(node);
                        }
                    } catch (Exception e) {
                        System.out.println("page.getText() = " + page.getText());
                        e.printStackTrace();
                        return;
                    }
                    writer.println();
                }
            });

            wxsp.parse();
        }catch(Exception e) {
            e.printStackTrace();
        }


    }
}
