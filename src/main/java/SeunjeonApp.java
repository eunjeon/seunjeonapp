import org.bitbucket.eunjeon.seunjeon.Analyzer;
import org.bitbucket.eunjeon.seunjeon.LNode;

import java.util.List;

/**
 * Created by parallels on 8/12/15.
 */
public class SeunjeonApp {
    public static void main(String args[]) {

        List<LNode> result;
        result = Analyzer.parseJava("하루하루 지나가면 익숙해질까.");
        System.out.println("result = " + result);

        result = Analyzer.parseJava("무궁화꽃이 피었습니다.");
        System.out.println("result = " + result);

    }
}
